window.addEventListener("DOMContentLoaded", function() {
    const url = "/upload";
    const form = document.querySelector('form');
    form.addEventListener('submit', e => {

        e.preventDefault();
    
        const files = document.querySelector('[name=file]').files;
        const progress = document.querySelector('[id=progress]');

        const file = files[0];
        const fileName = file.name;
        const mimeType = "image/jpeg"
    
        const xhr = new window.XMLHttpRequest();
    
        xhr.onload = () => {
            console.log(xhr.responseText);
        };
    
        xhr.open('POST', url, true);

        xhr.onprogress = function(e) {
            if (e.lengthComputable) {
              var percentage = (e.loaded / e.total) * 100;
              console.log(percentage + "%");
              progress.innerHTML = "Progress : " + percentage + "%";
            }
        };
        xhr.onerror = function(e) {
            console.log('Error');
            console.log(e);
        };
        xhr.onload = function() {
            console.log("LOADED");
            progress.innerHTML = "File Uploaded";
        };

        xhr.setRequestHeader('Content-Type', mimeType);
        
        xhr.send(file);
    });
}, false);

