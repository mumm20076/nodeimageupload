const http = require("http");
const path = require("path");
const fs = require("fs");
var cloudinary = require('cloudinary');
var datauri = require('datauri');

const bodyParser = require("body-parser");

const express = require("express");

const app = express();
const httpServer = http.createServer(app);

const HOSTNAME = "127.0.0.1"
const PORT = 4000;

httpServer.listen(process.env.PORT, () => {
  console.log(`Server is listening on http://${PORT}`);
});

app.use("/", express.static('./public/'));
app.get("/", express.static(path.join(__dirname, "./public")));

cloudinary.config({
  cloud_name: 'hgo9pdjyy',
  api_key: '523638738623698',
  api_secret: '5kFTLMn7PpubuCB7ZIclbU2zJzs'
});
// var dUri = new datauri();


const handleError = (err, res) => {
  res
    .status(500)
    .contentType("text/plain")
    .end("Oops! Something went wrong!");
};

app.post("/upload",
  bodyParser.raw({type: ["image/jpeg", "image/png"], limit: "10mb"}),
  (req, res) => {
    if(!req.body) {
        console.log('Body is nil');
    }
    else {
        console.log('Body is Present');
    }
    try {
      const filePath = __dirname+"/uploads/" + Date.now() + ".jpeg";
      console.log(req.body);
      fs.writeFile(filePath, req.body, (error) => {
        if (error) {
          throw error;
        }
        console.log("FILEPATH : "+filePath);
      });
      console.log('STARTING DATAURI');
      // datauri.format(path.extname(req.file.originalname).toString(),req.file.buffer);
      // datauri.format("abcd",req.file.buffer);
      console.log('STARTING DATAURI COMPLETED');
      cloudinary.uploader.upload(filePath, function (err, i) {
        if (err) {
          console.log('UPLOAD SUCCESSFULL');
          console.log(JSON.stringify(err));
        } else {
          console.log('UPLOAD FAILED');
          console.log("ERR : "+err);
        }
      });

      res.sendStatus(200);
    } catch (error) {
      res.sendStatus(500);
    }
  });
